
#include "env.h"

auto Environment::get(std::string name) -> std::optional<Object *> {
  if (auto obj = env.find(name); obj != env.end()) {
    /* found */
    return obj->second;
  } else {
    /* check if there is an outer env */
    if (this->outer != nullptr) {
      return this->outer->get(name);
    }
  }

  return {};
}

auto Environment::set(std::string name, Object *val) -> Object * {
  const auto [it, success] = env.insert({name, val});
  return it->second;
}

void Environment::print() {
  for (auto tmp : env) {
    std::cout << "env[" << tmp.first << "]: " << tmp.second->inspect() << std::endl;
  }
}
