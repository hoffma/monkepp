#pragma once
#include <vector>
#include <iostream>
#include <string_view>
#include <sstream>

class Environment;

#include "ast.h"

typedef std::string_view object_t;

namespace ObjectType {
  constexpr std::string_view INTEGER_OBJ{"INTEGER"};
  constexpr std::string_view BOOLEAN_OBJ{"BOOLEAN"};
  constexpr std::string_view NULL_OBJ{"NULL"};
  constexpr std::string_view RETURN_VALUE_OBJ{"RETURN_VALUE"};
  constexpr std::string_view ERROR_OBJ{"ERROR"};
  constexpr std::string_view FUNCTION_OBJ{"FUNCTION"};
  constexpr std::string_view STRING_OBJ{"STRING"};
};

class Object {
  public:
    Object() {}
    virtual auto type() -> object_t = 0;
    virtual auto inspect() -> std::string = 0;
    virtual ~Object() {}
};

class IntegerObj : public Object {
  public:
    IntegerObj(int64_t v) : value(v) {}

    int64_t value;

    auto type() -> object_t;
    auto inspect() -> std::string;
};

class BooleanObj : public Object {
  public:
    BooleanObj(bool v) : value(v) {}

    bool value;

    auto type() -> object_t;
    auto inspect() -> std::string;
};

class NullObj : public Object {
  public:
    auto type() -> object_t;
    auto inspect() -> std::string;
};

class ReturnValue : public Object {
  public:
    ReturnValue() {}
    ReturnValue(Object *v) : value(v) {}
    Object *value;

    auto type() -> object_t;
    auto inspect() ->std::string;
};

class ErrorObj : public Object {
  public:
    ErrorObj() {}
    ErrorObj(std::string &msg) : message(msg) {}
    std::string message;

    auto type() -> object_t;
    auto inspect() ->std::string;
};

class FunctionObj : public Object {
  public:
    FunctionObj(
        std::vector<Identifier *> params, BlockStatement *bd, 
        Environment* e) : parameters(params), body(bd), env(e) {}
    std::vector<Identifier *> parameters;
    BlockStatement * body;
    Environment *env;

    auto type() -> object_t;
    auto inspect() ->std::string;
};

class StringObj : public Object {
  public:
    StringObj(std::string val) : value(val) {}

    std::string value;

    auto type() -> object_t;
    auto inspect() ->std::string;
};
