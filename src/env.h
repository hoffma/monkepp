#pragma once

#include <iostream>
#include <map>
#include <optional>

#include "object.h"

class Environment {
  public:
    Environment(Environment *out = nullptr) : outer(out) {}

    auto get(std::string name) -> std::optional<Object *>;
    auto set(std::string name, Object *val) -> Object *;
    auto new_enclosed_env() -> Environment *;

    void print();

  private:
    std::map<std::string, Object *> env;
    Environment *outer;
};
