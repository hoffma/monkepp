
#include "parser.h"
#include <sstream>

void Parser::next_token() {
  _currToken = _peekToken;
  _peekToken = _lex.next_token();
}

auto Parser::parse_program() -> std::optional<Ast::Program> {
  auto program = Ast::Program{};

  while (_currToken.type != TokenType::EOFILE) {
    auto stmt = this->parse_statement();
    if (stmt.has_value()) {
      program.statements.push_back(stmt.value());
    }
    next_token(); /* skip semicolon */
  }
  return program;
}

auto Parser::parse_statement() -> std::optional<Statement *> {
  if (_currToken.type == TokenType::LET) {
    return parse_let_statement();
  } else if (_currToken.type == TokenType::RETURN) {
    return parse_return_statement();
  }
  return parse_expression_statement();
}

auto Parser::parse_let_statement() -> std::optional<LetStatement *> {
  auto stmt = new LetStatement{};
  stmt->token = _currToken;
  if (!expect_peek(TokenType::IDENT)) {
    return {};
  }
  stmt->name = new Identifier{_currToken, _currToken.literal};

  if (!expect_peek(TokenType::ASSIGN)) {
    return {};
  }

  next_token();
  auto tmp = parse_expression(Precedence::LOWEST);
  stmt->value = tmp.has_value() ? tmp.value() : nullptr;

  while (!curr_token_is(TokenType::SEMICOLON)) {
    this->next_token();
  }

  return stmt;
}

auto Parser::parse_return_statement() -> std::optional<ReturnStatement *> {
  auto stmt = new ReturnStatement{};
  stmt->token = _currToken;
  next_token();

  auto tmp = parse_expression(Precedence::LOWEST);
  stmt->retVal = tmp.has_value() ? tmp.value() : nullptr;

  while (!curr_token_is(TokenType::SEMICOLON)) {
    next_token();
  }

  return stmt;
}

auto Parser::parse_expression_statement()
    -> std::optional<ExpressionStatement *> {
  auto *stmt = new ExpressionStatement{};
  stmt->token = _currToken;

  auto tmp = parse_expression(Precedence::LOWEST);
  /* check if the expression was successfully parsed first */
  if (tmp.has_value()) {
    stmt->expr = tmp.value();

    if (peek_token_is(TokenType::SEMICOLON))
      next_token();
    return stmt;
  }
  return {};
}

auto Parser::parse_expression(Precedence p) -> std::optional<Expression *> {
  Expression *leftExp;
  std::optional<Expression *> tmp;
  if (auto prefix = prefixFnMap.find(_currToken.type);
      prefix != prefixFnMap.end()) {
    // found
    /*auto tmp = prefix->second();*/
    tmp = prefix->second();
    if (!tmp.has_value()) {
      no_prefix_parse_fn_error(_currToken);
      return {};
    }
  } else {
    return {};
  }

  leftExp = tmp.value();
  // auto state = !peek_token_is(TokenType::SEMICOLON) && (p < peek_precedence());
  while (!peek_token_is(TokenType::SEMICOLON) && (p < peek_precedence())) {
    if (auto infix = infixFnMap.find(_peekToken.type);
        infix != infixFnMap.end()) {
      // found
      next_token();
      leftExp = infix->second(leftExp).value();
    } else {
      return leftExp;
    }
  }

  return leftExp;
}

auto Parser::parse_identifier() -> std::optional<Expression *> {
  auto t = new Identifier{Token{_currToken.type, _currToken.literal},
                          _currToken.literal};
  return t;
}

auto Parser::parse_integer_literal() -> std::optional<Expression *> {
  auto *lit = new IntegerLiteral{_currToken};
  try {
    lit->value = std::stoi(_currToken.literal);
  } catch (const std::invalid_argument &e) {
    std::cerr << e.what();
    return {};
  }
  return lit;
}

auto Parser::parse_string_literal() -> std::optional<Expression*> {
  return new StringLiteral(_currToken, _currToken.literal);
}

auto Parser::parse_prefix_expression() -> std::optional<Expression *> {
  auto *expr = new PrefixExpression{_currToken, _currToken.literal};

  next_token();
  auto tmp = parse_expression(Precedence::PREFIX);
  if (tmp.has_value()) {
    expr->right = tmp.value();
  } else {
    expr->right = {};
  }
  return expr;
}

auto Parser::parse_infix_expression(Expression *left)
    -> std::optional<Expression *> {
  auto *expr = new InfixExpression(_currToken, _currToken.literal);
  expr->left = left;

  auto precedence = cur_precedence();
  next_token();
  auto tmp_right = parse_expression(precedence);
  if (tmp_right.has_value()) {
    expr->right = tmp_right.value();
  } else {
    expr->right = {};
  }
  return expr;
}

auto Parser::parse_boolean_expression() -> std::optional<Expression *> {
  auto *b = new BooleanLiteral{_currToken, curr_token_is(TokenType::TRUE)};
  return b;
}

auto Parser::parse_grouped_expression() -> std::optional<Expression *> {
  next_token();

  auto tmp = parse_expression(Precedence::LOWEST);
  if (!expect_peek(TokenType::RPAREN)) {
    return nullptr;
  }
  if (tmp.has_value()) {
    return tmp.value();
  }

  return nullptr;
}

auto Parser::parse_if_expression() -> std::optional<Expression *> {
  auto *expr = new IfExpression(_currToken);

  if (!expect_peek(TokenType::LPAREN)) {
    return {};
  }

  next_token();
  auto tmp_c = parse_expression(Precedence::LOWEST);
  expr->condition = tmp_c.has_value() ? tmp_c.value() : nullptr;

  if (!expect_peek(TokenType::RPAREN)) {
    return {};
  }
  if (!expect_peek(TokenType::LBRACE)) {
    return {};
  }

  auto tmp = parse_block_statement();
  expr->consequence =
      tmp.has_value() ? dynamic_cast<BlockStatement *>(tmp.value()) : nullptr;

  if (peek_token_is(TokenType::ELSE)) {
    next_token();
    if (!expect_peek(TokenType::LBRACE)) {
      return {};
    }

    auto tmp_a = parse_block_statement();
    expr->alternative = tmp_a.has_value()
                            ? dynamic_cast<BlockStatement *>(tmp_a.value())
                            : nullptr;
  }

  return expr;
}

auto Parser::parse_block_statement() -> std::optional<BlockStatement *> {
  auto *block = new BlockStatement(_currToken);

  next_token();

  while (!curr_token_is(TokenType::RBRACE) &&
         !curr_token_is(TokenType::EOFILE)) {
    auto stmt = parse_statement();
    if (stmt.has_value()) {
      block->statements.push_back(stmt.value());
    }
    next_token();
  }
  return block;
}

auto Parser::parse_function_literal() -> std::optional<Expression*> {
  auto *lit = new FunctionLiteral(_currToken);
  if (!expect_peek(TokenType::LPAREN)) {
    return {};
  }

  auto tmp_params = parse_function_parameters();
  if (tmp_params.has_value()) {
    lit->parameters = tmp_params.value();
  }

  if (!expect_peek(TokenType::LBRACE)) {
    return {};
  }

  auto tmp_block = parse_block_statement();
  lit->body = tmp_block.has_value() ? tmp_block.value() : nullptr;

  return lit;
}

auto Parser::parse_function_parameters() -> std::optional<std::vector<Identifier *>> {
  std::vector<Identifier*> identifiers;

  if (peek_token_is(TokenType::RPAREN)) { /* empty parameter list */
    next_token();
    return identifiers;
  }

  next_token();

  identifiers.push_back(new Identifier(_currToken, _currToken.literal));

  while (peek_token_is(TokenType::COMMA)) {
    next_token();
    next_token();
    identifiers.push_back(new Identifier(_currToken, _currToken.literal));
  }

  if (!expect_peek(TokenType::RPAREN)) {
    return {};
  }

  return identifiers;
}

auto Parser::parse_call_expression(Expression *fn) -> std::optional<Expression*> {
  auto *expr = new CallExpression(_currToken, fn);

  auto tmp_args = parse_call_arguments();
  if (tmp_args.has_value()) {
    expr->arguments = tmp_args.value();
  }

  return expr;
}

auto Parser::parse_call_arguments() -> std::optional<std::vector<Expression*>> {
  std::vector<Expression *> args;

  if (peek_token_is(TokenType::RPAREN)) {
    /* no args to parse */
    next_token();
    return args;
  }

  next_token();
  auto tmp_arg = parse_expression(Precedence::LOWEST);
  if (tmp_arg.has_value()) {
    args.push_back(tmp_arg.value());
  }

  while (peek_token_is(TokenType::COMMA)) {
    next_token();
    next_token();
    tmp_arg = parse_expression(Precedence::LOWEST);
    if (tmp_arg.has_value()) {
      args.push_back(tmp_arg.value());
    }
  }

  if (!expect_peek(TokenType::RPAREN)) {
    return {};
  }
  return args;
}

auto Parser::curr_token_is(token_t tt) -> bool { return _currToken.type == tt; }

auto Parser::peek_token_is(token_t tt) -> bool { return _peekToken.type == tt; }

auto Parser::expect_peek(token_t tt) -> bool {
  if (peek_token_is(tt)) {
    next_token();
    return true;
  }
  peek_error(tt);
  return false;
}

auto Parser::get_errors() -> std::vector<std::string> { return _errors; }

auto Parser::peek_precedence() -> Precedence {
  if (auto p = precedences.find(_peekToken.type); p != precedences.end()) {
    return p->second;
  }
  return Precedence::LOWEST;
}

auto Parser::cur_precedence() -> Precedence {
  if (auto p = precedences.find(_currToken.type); p != precedences.end()) {
    return p->second;
  }
  return Precedence::LOWEST;
}

void Parser::no_prefix_parse_fn_error(const Token &t) {
  std::stringstream tmp_s;
  tmp_s << "No prefix parse function for " << t.type << " found." << std::endl;
  _errors.push_back(tmp_s.str());
}

void Parser::peek_error(token_t t) {
  std::stringstream msg;
  msg << "Expected next token to be '" << t << "' got '" << _peekToken.type
      << "' instead";

  _errors.push_back(msg.str());
}

void Parser::register_prefix(token_t type, prefixFn fn) {
  this->prefixFnMap[type] = fn;
}

void Parser::register_infix(token_t type, infixFn fn) {
  this->infixFnMap[type] = fn;
}
