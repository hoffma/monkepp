#pragma once

#include <iostream>

#include "ast.h"
#include "object.h"
#include "env.h"

namespace evaluator {
[[nodiscard]] auto is_null(Object *obj) -> bool;
[[nodiscard]] auto eval(Ast::Program *prog, Environment *env) -> Object *;
[[nodiscard]] auto eval(Statement *node, Environment *env) -> Object *;
[[nodiscard]] auto eval(Expression *node, Environment *env) -> Object *;
[[nodiscard]] auto eval_program(std::vector<Statement *> stmts, Environment *env) -> Object *;
[[nodiscard]] auto eval_block_statement(BlockStatement *block, Environment *env) -> Object *;
[[nodiscard]] auto eval_prefix_expression(PrefixExpression *node, Environment *env) -> Object *;
[[nodiscard]] auto eval_infix_expression(InfixExpression *node, Environment *env) -> Object *;
[[nodiscard]] auto eval_if_expression(IfExpression *ie, Environment *env) -> Object *;
[[nodiscard]] auto eval_identifier(Identifier *node, Environment *env) -> Object *;
[[nodiscard]] auto eval_expressions(std::vector<Expression *> exps, Environment *env) -> std::vector<Object *>;
[[nodiscard]] auto apply_function(Object *fn, std::vector<Object *> &args) -> Object *;
[[nodiscard]] auto extend_function_env(FunctionObj *fn, std::vector<Object *> &args) -> Environment *;
[[nodiscard]] auto unwrap_return_value(Object *obj) -> Object *;
[[nodiscard]] auto eval_bang_operator_expr(Object *right) -> Object *;
[[nodiscard]] auto eval_minus_prefix_operator_expr(Object *right) -> Object *;
[[nodiscard]] auto eval_integer_infix_expression(std::string op, Object *left,
                                                 Object *right) -> Object *;
[[nodiscard]] auto eval_string_infix_expression(std::string op, Object *left,
                                                 Object *right) -> Object *;
[[nodiscard]] auto is_truthy(Object *obj) -> bool;
[[nodiscard]] auto new_error(std::string msg) -> ErrorObj *;
[[nodiscard]] auto is_error(Object *obj) -> bool;
} // namespace evaluator
