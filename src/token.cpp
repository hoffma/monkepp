#include "token.h"

auto Tok::lookup_ident(std::string ident) -> token_t {
  if (keywords.find(ident) == keywords.end()) {
    return TokenType::IDENT;
  } else {
    return keywords.at(ident);
  }
}


std::ostream& operator<<(std::ostream &os, const Token &tok) {
  os << "Token { type: " << tok.type << ", literal: " << tok.literal << " }";
  return os;
}
