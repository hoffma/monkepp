#include "object.h"

using namespace ObjectType;

/*########## Integer ##########*/
auto IntegerObj::inspect() -> std::string {
  std::stringstream out;
  out << value;
  return out.str();
}

auto IntegerObj::type() -> object_t {
  return INTEGER_OBJ;
}

/*########## Boolean ##########*/
auto BooleanObj::inspect() -> std::string {
  std::stringstream out;
  out << value;
  return out.str();
}

auto BooleanObj::type() -> object_t {
  return BOOLEAN_OBJ;
}

/*########## Null ##########*/
auto NullObj::inspect() -> std::string {
  return std::string{"null"};
}

auto NullObj::type() -> object_t {
  return NULL_OBJ;
}


/*########## Return Value ##########*/
auto ReturnValue::type() -> object_t {
  return RETURN_VALUE_OBJ;
}

auto ReturnValue::inspect() ->std::string {
  return value->inspect();
}

/*########## Error ##########*/
auto ErrorObj::type() -> object_t {
  return ObjectType::ERROR_OBJ;
}

auto ErrorObj::inspect() ->std::string {
  std::stringstream ret;
  ret << "ERROR: " << message;
  return ret.str();
}

/*########## Function ##########*/
auto FunctionObj::type() -> object_t {
  return ObjectType::FUNCTION_OBJ;
}

auto FunctionObj::inspect() ->std::string {
  std::stringstream out;

  out << "fn(";
  for(size_t i = 0; i < parameters.size(); ++i) {
    out << parameters[i]->to_string();
    out << (i < (parameters.size()-1) ? ", " : "");
  }
  out << ")";
  out << body->to_string();

  return out.str();
}

/*########## String ##########*/
auto StringObj::type() -> object_t {
  return ObjectType::STRING_OBJ;
}

auto StringObj::inspect() ->std::string {
  return value;
}
