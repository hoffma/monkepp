#include "ast.h"

auto token_literal(Ast::Program &p) -> std::string {
  if (p.statements.size() > 0) {
    return p.statements[0]->token_literal();
  } else {
    return "";
  }
}

auto LetStatement::token_literal() -> std::string {
  return token.literal;
  /*return "hello";*/
}

auto Identifier::token_literal() -> std::string {
  return token.literal;
}

std::ostream& operator<<(std::ostream &os, const NodeType &nt) {
  switch (nt) {
    case NodeType::NODE:
      os << "NODE";
      break;
    case NodeType::STATEMENT:
      os << "STATEMENT";
      break;
    case NodeType::EXPRESSION:
      os << "EXPRESSION";
      break;
    case NodeType::IDENTIFIER:
      os << "IDENTIFIER";
      break;
    case NodeType::LET_STATEMENT:
      os << "LET_STATEMENT";
      break;
    case NodeType::RETURN_STATEMENT:
      os << "RETURN_STATEMENT";
      break;
    case NodeType::EXPRESSION_STATEMENT:
      os << "EXPRESSION_STATEMENT";
      break;
    case NodeType::BLOCK_STATEMENT:
      os << "BLOCK_STATEMENT";
      break;
    case NodeType::INTEGER_LITERAL:
      os << "INTEGER_LITERAL";
      break;
    case NodeType::STRING_LITERAL:
      os << "STRING_LITERAL";
      break;
    case NodeType::PREFIX_EXPRESION:
      os << "PREFIX_EXPRESION";
      break;
    case NodeType::INFIX_EXPRESSION:
      os << "INFIX_EXPRESSION";
      break;
    case NodeType::BOOLEAN_LITERAL:
      os << "BOOLEAN_LITERAL";
      break;
    case NodeType::IF_EXPRESSION:
      os << "IF_EXPRESSION";
      break;
    case NodeType::FUNCTION_LITERAL:
      os << "FUNCTION_LITERAL";
      break;
    case NodeType::CALL_EXPRESSION:
      os << "CALL_EXPRESSION";
      break;
  }

  return os;
}

std::ostream &operator<<(std::ostream &os, Statement &n) {
  os << n.node_type;
  return os;
}
std::ostream &operator<<(std::ostream &os, Expression &n) {
  os << n.node_type;
  return os;
}
