#pragma once

#include <iostream>
#include <optional>
#include <functional>
#include <vector>
#include "ast.h"
#include "lexer.h"
#include "token.h"

using namespace std::placeholders;

enum class Precedence {
  LOWEST,
  EQUALS,
  LESSGREATER,
  SUM,
  PRODUCT,
  PREFIX,
  CALL
};

inline std::string precedence_to_string(Precedence p) {
  switch(p) {
    case Precedence::LOWEST:
      return "Precedence::LOWEST";
    case Precedence::EQUALS:
      return "Precedence::EQUALS"; 
    case Precedence::LESSGREATER:
      return "Precedence::LESSGREATER";
    case Precedence::SUM:
      return "Precedence::SUM";
    case Precedence::PRODUCT:
      return "Precedence::PRODUCT";
    case Precedence::PREFIX:
      return "Precedence::PREFIX";
    case Precedence::CALL:
      return "Precedence::CALL";
    default:
      return "Precedence not found";
  }
}

class Parser {
  typedef std::function<std::optional<Expression*>()> prefixFn;
  typedef std::function<std::optional<Expression*>(Expression *)> infixFn;

  public:
    Parser(const Lexer &l) : _lex(l) {
      /* Read two tokens, so curToken and peekToken are both set */
      // next_token();
      // next_token();
      _currToken = _lex.next_token();
      _peekToken = _lex.next_token();
      register_prefix(TokenType::IDENT, std::bind(&Parser::parse_identifier, this));
      register_prefix(TokenType::INT, std::bind(&Parser::parse_integer_literal, this));
      register_prefix(TokenType::STRING, std::bind(&Parser::parse_string_literal, this));
      /* prefix expressions */
      register_prefix(TokenType::BANG, std::bind(&Parser::parse_prefix_expression, this));
      register_prefix(TokenType::MINUS, std::bind(&Parser::parse_prefix_expression, this));
      register_prefix(TokenType::TRUE, std::bind(&Parser::parse_boolean_expression, this));
      register_prefix(TokenType::FALSE, std::bind(&Parser::parse_boolean_expression, this));
      register_prefix(TokenType::LPAREN, std::bind(&Parser::parse_grouped_expression, this));
      register_prefix(TokenType::IF, std::bind(&Parser::parse_if_expression, this));
      register_prefix(TokenType::FUNCTION, std::bind(&Parser::parse_function_literal, this));
      /* infix expressions */
      register_infix(TokenType::PLUS, std::bind(&Parser::parse_infix_expression, this, _1));
      register_infix(TokenType::MINUS, std::bind(&Parser::parse_infix_expression, this, _1));
      register_infix(TokenType::SLASH, std::bind(&Parser::parse_infix_expression, this, _1));
      register_infix(TokenType::ASTERISK, std::bind(&Parser::parse_infix_expression, this, _1));
      register_infix(TokenType::EQ, std::bind(&Parser::parse_infix_expression, this, _1));
      register_infix(TokenType::NOT_EQ, std::bind(&Parser::parse_infix_expression, this, _1));
      register_infix(TokenType::LT, std::bind(&Parser::parse_infix_expression, this, _1));
      register_infix(TokenType::GT, std::bind(&Parser::parse_infix_expression, this, _1));
      register_infix(TokenType::LPAREN, std::bind(&Parser::parse_call_expression, this, _1));
    }
    void next_token();
    [[nodiscard]] auto parse_program() -> std::optional<Ast::Program>;
    [[nodiscard]] auto parse_statement() -> std::optional<Statement*>;
    [[nodiscard]] auto parse_let_statement() -> std::optional<LetStatement*>;
    [[nodiscard]] auto parse_return_statement() -> std::optional<ReturnStatement*>;
    [[nodiscard]] auto parse_expression_statement() -> std::optional<ExpressionStatement*>;
    [[nodiscard]] auto parse_expression(Precedence p) -> std::optional<Expression*>;
    [[nodiscard]] auto parse_identifier() -> std::optional<Expression*>;
    [[nodiscard]] auto parse_integer_literal() -> std::optional<Expression*>;
    [[nodiscard]] auto parse_string_literal() -> std::optional<Expression*>;
    [[nodiscard]] auto parse_prefix_expression() -> std::optional<Expression*>;
    [[nodiscard]] auto parse_infix_expression(Expression *left) -> std::optional<Expression*>;
    [[nodiscard]] auto parse_boolean_expression() -> std::optional<Expression*>;
    [[nodiscard]] auto parse_grouped_expression() -> std::optional<Expression*>;
    [[nodiscard]] auto parse_if_expression() -> std::optional<Expression*>;
    [[nodiscard]] auto parse_block_statement() -> std::optional<BlockStatement *>;
    [[nodiscard]] auto parse_function_literal() -> std::optional<Expression*>;
    [[nodiscard]] auto parse_function_parameters() -> std::optional<std::vector<Identifier *>>;
    [[nodiscard]] auto parse_call_expression(Expression *fn) -> std::optional<Expression*>;
    [[nodiscard]] auto parse_call_arguments() -> std::optional<std::vector<Expression*>>;
    [[nodiscard]] auto curr_token_is(token_t tt) -> bool;
    [[nodiscard]] auto peek_token_is(token_t tt) -> bool;
    [[nodiscard]] auto expect_peek(token_t tt) -> bool;
    [[nodiscard]] auto get_errors() -> std::vector<std::string>;
    [[nodiscard]] auto peek_precedence() -> Precedence;
    [[nodiscard]] auto cur_precedence() -> Precedence;
    void no_prefix_parse_fn_error(const Token &t);
    void peek_error(token_t t);

  private:
    void register_prefix(token_t type, prefixFn fn);
    void register_infix(token_t type, infixFn fn);
    Lexer _lex;
    Token _currToken;
    Token _peekToken;
    std::vector<std::string> _errors;

    std::map<token_t, prefixFn> prefixFnMap;
    std::map<token_t, infixFn> infixFnMap;

    const std::map<token_t, Precedence> precedences {
      {TokenType::EQ,     Precedence::EQUALS},
      {TokenType::NOT_EQ, Precedence::EQUALS},
      {TokenType::LT,     Precedence::LESSGREATER},
      {TokenType::GT,     Precedence::LESSGREATER},
      {TokenType::PLUS,   Precedence::SUM},
      {TokenType::MINUS,  Precedence::SUM},
      {TokenType::SLASH,  Precedence::PRODUCT},
      {TokenType::ASTERISK, Precedence::PRODUCT},
      {TokenType::LPAREN, Precedence::CALL},
    };
};
