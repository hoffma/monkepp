#pragma once

#include <iostream>
#include <string_view>
#include <map>

namespace TokenType {
  constexpr std::string_view ILLEGAL{"ILLEGAL"};
  constexpr std::string_view EOFILE{"EOF"};
  constexpr std::string_view IDENT{"IDENT"};
  constexpr std::string_view INT{"INT"};
  constexpr std::string_view ASSIGN{"="};
  constexpr std::string_view PLUS{"+"};
  constexpr std::string_view MINUS{"-"};
  constexpr std::string_view BANG{"!"};
  constexpr std::string_view ASTERISK{"*"};
  constexpr std::string_view SLASH{"/"};
  constexpr std::string_view LT{"<"};
  constexpr std::string_view GT{">"};
  constexpr std::string_view COMMA{","};
  constexpr std::string_view SEMICOLON{";"};
  constexpr std::string_view LPAREN{"("};
  constexpr std::string_view RPAREN{")"};
  constexpr std::string_view LBRACE{"{"};
  constexpr std::string_view RBRACE{"}"};
  constexpr std::string_view EQ{"=="};
  constexpr std::string_view NOT_EQ{"!="};
  constexpr std::string_view FUNCTION{"FUNCTION"};
  constexpr std::string_view LET{"LET"};
  constexpr std::string_view TRUE{"TRUE"};
  constexpr std::string_view FALSE{"FALSE"};
  constexpr std::string_view IF{"IF"};
  constexpr std::string_view ELSE{"ELSE"};
  constexpr std::string_view RETURN{"RETURN"};
  constexpr std::string_view STRING{"STRING"};
};

typedef std::string_view token_t;

const std::map<std::string, token_t> keywords {
  {"fn", TokenType::FUNCTION},
  {"let", TokenType::LET},
  {"true", TokenType::TRUE},
  {"false", TokenType::FALSE},
  {"if", TokenType::IF},
  {"else", TokenType::ELSE},
  {"return", TokenType::RETURN},
};

namespace Tok {
  auto lookup_ident(std::string ident) -> token_t;
};

struct Token {
  token_t type;
  std::string literal;

  friend std::ostream& operator<<(std::ostream &os, const Token &tok);
};

