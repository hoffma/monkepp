#include "lexer.h"

auto Lexer::next_token() -> Token {
  std::string tmp;
  Token tok{TokenType::ILLEGAL, "ILLEGAL"};

  skip_whitespace();

  switch(_ch) {
    case '=':
      if (peek_char() == '=') {
        tmp = _ch;
        read_char();
        tmp += _ch;
        tok = Token{TokenType::EQ, tmp};
      } else {
        tok = new_token(TokenType::ASSIGN, _ch);
      }
      break;
    case '(':
      tok = new_token(TokenType::LPAREN, _ch);
      break;
    case ')':
      tok = new_token(TokenType::RPAREN, _ch);
      break;
    case '{':
      tok = new_token(TokenType::LBRACE, _ch);
      break;
    case '}':
      tok = new_token(TokenType::RBRACE, _ch);
      break;
    case '+':
      tok = new_token(TokenType::PLUS, _ch);
      break;
    case '-':
      tok = new_token(TokenType::MINUS, _ch);
      break;
    case '!':
      if (peek_char() == '=') {
        tmp = _ch;
        read_char();
        tmp += _ch;
        tok = Token{TokenType::NOT_EQ, tmp};
      } else {
        tok = new_token(TokenType::BANG, _ch);
      }
      break;
    case '*':
      tok = new_token(TokenType::ASTERISK, _ch);
      break;
    case '/':
      tok = new_token(TokenType::SLASH, _ch);
      break;
    case '<':
      tok = new_token(TokenType::LT, _ch);
      break;
    case '>':
      tok = new_token(TokenType::GT, _ch);
      break;
    case ';':
      tok = new_token(TokenType::SEMICOLON, _ch);
      break;
    case ',':
      tok = new_token(TokenType::COMMA, _ch);
      break;
    case '"':
      tok.type = TokenType::STRING;
      tok.literal = read_string();
      break;
    case 0:
      tok = new_token(TokenType::EOFILE, '\0');
      break;
    default:
      if (is_letter(_ch)) {
        tok.literal = read_identifier();
        tok.type = Tok::lookup_ident(tok.literal);
        return tok;
      } else if (is_digit(_ch)) {
        tok.type = TokenType::INT;
        tok.literal = read_number();
        return tok;
      } else {
        tok = new_token(TokenType::ILLEGAL, _ch);
      }
  }

  this->read_char();
  return tok;
}

auto Lexer::read_identifier() -> std::string {
  auto position = _position;
  while(isalpha(_ch)) {
    this->read_char();
  }
  return _input.substr(position, (_position-position));
}

auto Lexer::is_letter(char c) -> bool {
  auto is_letter = ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') || c == '_';
  return is_letter;
}

auto Lexer::is_digit(char c) -> bool {
  return (('0' <= c) && (c <= '9'));
}

auto Lexer::read_number() -> std::string {
  auto position = _position;
  while (is_digit(_ch)) {
    read_char();
  }
  return _input.substr(position, (_position-position));
}

auto Lexer::new_token(token_t type, char byte) -> Token {
  return Token{type, std::string{byte}};
}

void Lexer::read_char() {
  if(_read_position >= _input.size()) {
    _ch = 0;
  } else {
    _ch = _input.at(_read_position);
  }
  _position = _read_position;
  _read_position++;
}

auto Lexer::peek_char() -> char {
  if (_read_position >= _input.size()) {
    return 0;
  } else {
    return _input[_read_position];
  }
}

void Lexer::skip_whitespace() {
  while((_ch == ' ') || (_ch == '\t') || (_ch == '\n') || (_ch == '\r')) {
    this->read_char();
  }
}

auto Lexer::read_string() -> std::string {
  auto position = _position + 1;
  while(1) {
    read_char();
    if ((_ch == '"') || _ch == 0) {
      break;
    }
  }

  return _input.substr(position, (_position-position));
}
