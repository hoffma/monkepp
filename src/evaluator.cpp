
#include "evaluator.h"

BooleanObj _TRUE{true};
BooleanObj _FALSE{false};
NullObj _NULL{};

auto evaluator::is_null(Object *obj) -> bool {
  return (obj == &_NULL);
}

auto native_bool_obj(bool input) -> Object * {
  return (input ? &_TRUE : &_FALSE);
}

auto evaluator::eval(Ast::Program *prog, Environment *env) -> Object * {
  auto *tmp = dynamic_cast<Ast::Program *>(prog);
  return eval_program(tmp->statements, env);
}

auto evaluator::eval(Statement *node, Environment *env) -> Object * {
  Object *val;
  LetStatement *tmp_ls;

  switch (node->node_type) {
  case NodeType::EXPRESSION_STATEMENT:
    return eval(dynamic_cast<ExpressionStatement *>(node)->expr, env);
  case NodeType::BLOCK_STATEMENT:
    return eval_block_statement(dynamic_cast<BlockStatement *>(node), env);
  case NodeType::RETURN_STATEMENT:
    val = eval(dynamic_cast<ReturnStatement *>(node)->retVal, env);
    if (is_error(val)) {
      return val;
    }
    return new ReturnValue{val};
  case NodeType::LET_STATEMENT:
    tmp_ls = dynamic_cast<LetStatement *>(node);
    val = eval(tmp_ls->value, env);
    if (is_error(val)) {
      return val;
    }
    env->set(tmp_ls->name->value, val);
    /*return val;*/
    break;
  default:
    return nullptr;
  }

  return nullptr;
}

auto evaluator::eval(Expression *node, Environment *env) -> Object * {
  Expression *tmp;
  Object *ret;

  switch (node->node_type) {
  case NodeType::INTEGER_LITERAL:
    return new IntegerObj{dynamic_cast<IntegerLiteral *>(node)->value};
  case NodeType::BOOLEAN_LITERAL:
    return native_bool_obj(dynamic_cast<BooleanLiteral *>(node)->value);
  case NodeType::PREFIX_EXPRESION:
    tmp = dynamic_cast<PrefixExpression *>(node)->right;
    {
      auto val = eval(tmp, env);
      if (is_error(val)) return val;
    }
    return eval_prefix_expression(dynamic_cast<PrefixExpression *>(node), env);
  case NodeType::INFIX_EXPRESSION:
    {
      /* test left */
      tmp = dynamic_cast<InfixExpression *>(node)->left;
      auto left = eval(tmp, env);
      if (is_error(left)) return left;
      /* test right */
      tmp = dynamic_cast<InfixExpression *>(node)->right;
      auto right = eval(tmp, env);
      if (is_error(right)) return right;
    }
    ret = eval_infix_expression(dynamic_cast<InfixExpression *>(node), env);
    return ret;
  case NodeType::IF_EXPRESSION:
    {
      tmp = dynamic_cast<IfExpression *>(node)->condition;
      auto condition = eval(tmp, env);
      if (is_error(condition)) return condition;
    }
    ret = eval_if_expression(dynamic_cast<IfExpression *>(node), env);
    return ret;
  case NodeType::IDENTIFIER:
    return eval_identifier(dynamic_cast<Identifier *>(node), env);
  case NodeType::FUNCTION_LITERAL:
    {
      auto *fn = dynamic_cast<FunctionLiteral *>(node);
      return new FunctionObj{fn->parameters, fn->body, env};
    }
  case NodeType::CALL_EXPRESSION: 
    {
      auto *ce = dynamic_cast<CallExpression *>(node);
      auto function = eval(ce->function, env);
      if (is_error(function)) {
        return function;
      }
      auto args = eval_expressions(ce->arguments, env);
      if (args.size() == 1 && is_error(args[0])) {
        return args[0];
      }

      return apply_function(function, args);
    }
    break;
  case NodeType::STRING_LITERAL:
    {
      auto str_node = dynamic_cast<StringLiteral *>(node);
      return new StringObj(str_node->value);
    }
  default:
    return nullptr;
  }

  return nullptr;
}

auto evaluator::eval_program(std::vector<Statement *> stmts, Environment *env) -> Object * {
  Object *result = nullptr;

  for (auto &stmt : stmts) {
    result = evaluator::eval(stmt, env);

    if (result == nullptr) { /* probably a let statement */
      continue;
    } else if (result->type() == ObjectType::RETURN_VALUE_OBJ) {
      auto *tmp = dynamic_cast<ReturnValue *>(result);
      if (tmp != nullptr) {
        return tmp->value;
      }
    } else if (result->type() == ObjectType::ERROR_OBJ) {
      auto *tmp = dynamic_cast<ErrorObj *>(result);
      if (tmp != nullptr) {
        return tmp;
      }
    }
    delete stmt;
  }

  return result;
}

auto evaluator::eval_block_statement(BlockStatement *block, Environment *env) -> Object * {
  Object *result = nullptr;

  for (auto &stmt : block->statements) {
    result = eval(stmt, env);

    if (result != nullptr) {
      auto rt = result->type();
      if (rt == ObjectType::RETURN_VALUE_OBJ || rt == ObjectType::ERROR_OBJ) {
        return result;
      }
    }
  }

  return result;
}

auto evaluator::eval_prefix_expression(PrefixExpression *node, Environment *env) -> Object * {
  auto *right = eval(node->right, env);
  if (node->op == "!") {
    return eval_bang_operator_expr(right);
  } else if (node->op == "-") {
    return eval_minus_prefix_operator_expr(right);
  }

  std::stringstream err;
  err << "unknown operator: " << node->op << node->right->node_type;
  return new_error(err.str());
}

auto evaluator::eval_infix_expression(InfixExpression *node, Environment *env) -> Object * {
  auto op = node->op;
  auto *left = eval(node->left, env);
  auto *right = eval(node->right, env);

  if (left->type() == ObjectType::INTEGER_OBJ &&
      right->type() == ObjectType::INTEGER_OBJ) {
    return eval_integer_infix_expression(op, left, right);
  } else if (left->type() == ObjectType::STRING_OBJ &&
      right->type() == ObjectType::STRING_OBJ) {
    return eval_string_infix_expression(op, left, right);
  } else if (left->type() != right->type()) {
    std::stringstream err;
    err << "type mismatch: " << left->type() << " " << op << " " << right->type();
    return new_error(err.str());
  } else if (op == "==") {
    auto res = native_bool_obj(left == right);
    return res;
  } else if (op == "!=") {
    auto res = native_bool_obj(left != right);
    return res;
  }

  std::stringstream err;
  err << "unknown operator: " << left->type() << " " << op << " " << right->type();
  return new_error(err.str());
}

auto evaluator::eval_if_expression(IfExpression *ie, Environment *env) -> Object * {
  auto *condition = eval(ie->condition, env);

  if (is_truthy(condition)) {
    auto res = eval(ie->consequence, env);
    return res;
  } else if (ie->alternative) {
    auto res = eval(ie->alternative, env);
    return res;
  } else {
    return &_NULL;
  }
}

auto evaluator::eval_identifier(Identifier *node, Environment *env) -> Object * {
  auto val = env->get(node->value);
  if (!val.has_value()) {
    std::stringstream err;
    err << "identifier not found: " << node->value;
    return new_error(err.str());
  }
  return val.value();
}

auto evaluator::eval_expressions(std::vector<Expression *> exps, Environment *env) -> std::vector<Object *> {
  std::vector<Object *> results;
  for (auto &e: exps) {
    auto *evaluated = eval(e, env);
    if (is_error(evaluated)) {
      return std::vector<Object *>{evaluated};
    }
    results.push_back(evaluated);
  }

  return results;
}

auto evaluator::apply_function(Object *fn, std::vector<Object *> &args) -> Object * {
  auto function = dynamic_cast<FunctionObj *>(fn);
  if (function == nullptr) {
    std::stringstream ss;
    ss << "not a function: " << fn->type();
    return new_error(ss.str());
  }

  auto *extended_env = extend_function_env(function ,args);
  auto *evaluated = eval(function->body, extended_env);

  return unwrap_return_value(evaluated);
  return nullptr;
}

auto evaluator::extend_function_env(FunctionObj *fn, std::vector<Object *> &args) -> Environment * {
  auto *env = new Environment(fn->env);

  for (size_t i = 0; i < fn->parameters.size(); ++i) {
    env->set(fn->parameters[i]->value, args[i]);
  }

  return env;
}

auto evaluator::unwrap_return_value(Object *obj) -> Object * {
  auto ret_val = dynamic_cast<ReturnValue *>(obj);
  if (ret_val != nullptr) {
    return ret_val->value;
  }

  return obj;
}

auto evaluator::eval_bang_operator_expr(Object *right) -> Object * {
  if (right == &_TRUE)
    return &_FALSE;
  else if (right == &_FALSE)
    return &_TRUE;
  else if (right == &_NULL)
    return &_TRUE;
  else
    return &_FALSE;
}

auto evaluator::eval_minus_prefix_operator_expr(Object *right) -> Object * {
  if (right->type() != ObjectType::INTEGER_OBJ) {
    std::stringstream err;
    err << "unknown operator: -" << right->type();
    return new_error(err.str());
  }

  auto value = dynamic_cast<IntegerObj *>(right)->value;
  return new IntegerObj(-value);
}

auto evaluator::eval_integer_infix_expression(std::string op, Object *left,
                                              Object *right) -> Object * {

  int64_t left_val = dynamic_cast<IntegerObj *>(left)->value;
  int64_t right_val = dynamic_cast<IntegerObj *>(right)->value;

  if (op == "+") {
    return new IntegerObj{left_val + right_val};
  } else if (op == "-") {
    return new IntegerObj{left_val - right_val};
  } else if (op == "*") {
    return new IntegerObj{left_val * right_val};
  } else if (op == "/") {
    return new IntegerObj{left_val / right_val};
  } else if (op == "<") {
    return native_bool_obj(left_val < right_val);
  } else if (op == ">") {
    return native_bool_obj(left_val > right_val);
  } else if (op == "==") {
    return native_bool_obj(left_val == right_val);
  } else if (op == "!=") {
    return native_bool_obj(left_val != right_val);
  }

  std::stringstream err;
  err << "unknown operator: -" << right->type();
  return new_error(err.str());
  /*return &_NULL;*/
}

auto evaluator::eval_string_infix_expression(std::string op, Object *left, 
                                  Object *right) -> Object * {
  if (op != "+") {
    std::stringstream ss;
    ss << "unknown operator: " << left->type() << " " << op << " " 
      << right->type();
    return evaluator::new_error(ss.str());
  }

  auto left_val = dynamic_cast<StringObj *>(left)->value;
  auto right_val = dynamic_cast<StringObj *>(right)->value;

  return new StringObj{std::string{left_val + right_val}};
}

auto evaluator::is_truthy(Object *obj) -> bool {
  if (obj == &_NULL) {
    return false;
  } else if(obj == &_TRUE) {
    return true;
  } else if(obj == &_FALSE) {
    return false;
  } else {
    return true;
  }
}

auto evaluator::new_error(std::string msg) -> ErrorObj * {
  return new ErrorObj{msg};
}

auto evaluator::is_error(Object *obj) -> bool {
  if (obj != nullptr) {
    return obj->type() == ObjectType::ERROR_OBJ;
  }

  return false;
}
