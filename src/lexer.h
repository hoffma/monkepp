#pragma once

#include <iostream>
#include "token.h"

class Lexer {
  public:
    Lexer() 
      : _input(nullptr), _position(0), _read_position(1) {
        _ch = '\0';
      }
    Lexer(std::string src) 
      : _input(src), _position(0), _read_position(1) {
        _ch = src.at(0);
      }

    [[nodiscard]] auto next_token() -> Token;

  private:
    [[nodiscard]] auto new_token(token_t type, char byte) -> Token;
    [[nodiscard]] auto read_identifier() -> std::string;
    void read_char();
    [[nodiscard]] auto peek_char() -> char;
    void skip_whitespace();

    [[nodiscard]] auto is_letter(char c) -> bool;
    [[nodiscard]] auto is_digit(char c) -> bool;
    [[nodiscard]] auto read_number() -> std::string;
    [[nodiscard]] auto read_string() -> std::string;

    std::string _input;
    size_t _position;
    size_t _read_position;
    char _ch;
};
