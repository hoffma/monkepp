#pragma once
#include <iostream>
#include <sstream>
#include <vector>
#include <typeinfo>
#include "token.h"

enum class NodeType {
  NODE,
  STATEMENT,
  EXPRESSION,
  IDENTIFIER,
  LET_STATEMENT,
  RETURN_STATEMENT,
  EXPRESSION_STATEMENT,
  BLOCK_STATEMENT,
  INTEGER_LITERAL,
  STRING_LITERAL,
  PREFIX_EXPRESION,
  INFIX_EXPRESSION,
  BOOLEAN_LITERAL,
  IF_EXPRESSION,
  FUNCTION_LITERAL,
  CALL_EXPRESSION,
};

std::ostream& operator<<(std::ostream &os, const NodeType &nt);

class Node {
  public:
    Node() {}
    virtual auto token_literal() -> std::string = 0;
    virtual auto to_string() -> std::string = 0;
    virtual ~Node() {}
    const NodeType node_type = NodeType::NODE;
};

class Statement : public Node {
  public:
    Statement(NodeType nt) : node_type(nt) {}
    virtual void statement_node() = 0;
    virtual auto token_literal() -> std::string = 0;
    const NodeType node_type = NodeType::STATEMENT;
    /*~Statement() {}*/
    /*auto to_string() -> std::string {
      std::stringstream tmp_s;
      tmp_s << "Statement";
      return tmp_s.str();
    }*/
};

class Expression : public Node {
  public:
    Expression(NodeType nt) : node_type(nt) {}
    virtual void expression_node() = 0;
    virtual auto token_literal() -> std::string = 0;
    const NodeType node_type = NodeType::EXPRESSION;
    /*~Expression(){}*/
    /*auto to_string() -> std::string {
      std::stringstream tmp_s;
      tmp_s << "Expression";
      return tmp_s.str();
    }*/
};

class Identifier : public Expression {
  public:
    Identifier() : Expression(NodeType::IDENTIFIER) {}
    Identifier(Token t, std::string v) 
      : Expression(NodeType::IDENTIFIER), token(t), value(v) {}

    Token token;
    std::string value;

    void expression_node() {}
    auto token_literal() -> std::string;
    auto to_string() -> std::string { return value; }
};

class LetStatement : public Statement {
  public:
    LetStatement() : Statement(NodeType::LET_STATEMENT) {}
    LetStatement(Token t, Identifier *i, Expression *e) :
      Statement(NodeType::LET_STATEMENT), token(t), name(i), value(e) {}

    Token token;
    Identifier *name;
    Expression *value;

    /*LetStatement(Token t, Identifier n, Expression v) :
      token(t), name(n), value(v) {}*/

    void statement_node() {}
    auto token_literal() -> std::string;
    auto to_string() -> std::string {
      std::stringstream tmp_s;
      tmp_s << token_literal() << " " << name->to_string() << " = ";
      tmp_s << value->to_string() << ";";
      return tmp_s.str();
    }
};

class ReturnStatement : public Statement {
  public:
    ReturnStatement() : Statement(NodeType::RETURN_STATEMENT) {}

    Token token;
    Expression *retVal;

    void statement_node() {}
    auto token_literal() -> std::string { return token.literal; }
    auto to_string() -> std::string {
      std::stringstream tmp_s;
      tmp_s << token_literal() << " " << token.literal << " ";
      tmp_s << retVal->to_string() << ";";
      return tmp_s.str();
    }
};

class ExpressionStatement : public Statement {
  public:
    ExpressionStatement() : Statement(NodeType::EXPRESSION_STATEMENT) {}

    Token token;
    Expression *expr;

    void statement_node() {}
    auto token_literal() -> std::string { return token.literal; }
    auto to_string() -> std::string { return expr->to_string(); }
};


class BlockStatement : public Statement {
  public:
    BlockStatement(Token t) : Statement(NodeType::BLOCK_STATEMENT), token(t) {
      statements = std::vector<Statement *>{};
    }

    Token token;
    std::vector<Statement *> statements;

    void statement_node() {}
    auto token_literal() -> std::string { return token.literal; }
    auto to_string() -> std::string {
      std::stringstream s_tmp;

      for (auto &stmt : statements) {
        s_tmp << stmt->to_string();
      }

      return s_tmp.str();
    }
};

class IntegerLiteral : public Expression {
  public:
    IntegerLiteral(Token t, int64_t v) : Expression(NodeType::INTEGER_LITERAL), token(t), value(v) {}
    IntegerLiteral(Token t) : Expression(NodeType::INTEGER_LITERAL), token(t) {}

    Token token;
    int64_t value;

    void expression_node() {}
    auto token_literal() -> std::string { return token.literal; };
    auto to_string() -> std::string { return token.literal; }
};

class StringLiteral : public Expression {
  public:
    StringLiteral(Token t, std::string v) 
      : Expression(NodeType::STRING_LITERAL), token(t), value(v) {}
    StringLiteral(Token t) 
      : Expression(NodeType::STRING_LITERAL), token(t) {}

    Token token;
    std::string value;

    void expression_node() {}
    auto token_literal() -> std::string { return token.literal; };
    auto to_string() -> std::string { return token.literal; }
};

class PrefixExpression : public Expression {
  public:
    PrefixExpression(Token t, std::string l) 
      : Expression(NodeType::PREFIX_EXPRESION), token(t), op(l) {}

    Token token;
    std::string op;
    Expression *right;

    void expression_node(){};
    auto token_literal() -> std::string { return token.literal; };
    /*~Expression(){}*/
    auto to_string() -> std::string {
      std::stringstream tmp_s;
      tmp_s << "(" << op << right->to_string() << ")";
      return tmp_s.str();
    }
};

class InfixExpression : public Expression {
  public:
    InfixExpression(Token t, std::string l) 
      : Expression(NodeType::INFIX_EXPRESSION), token(t), op(l) {}

    Token token;
    Expression *left;
    std::string op;
    Expression *right;

    void expression_node(){};
    auto token_literal() -> std::string { return token.literal; };
    /*~Expression(){}*/
    auto to_string() -> std::string {
      std::stringstream tmp_s;
      tmp_s << "(" << left->to_string() << ' ' << op << ' ' << right->to_string() << ")";
      return tmp_s.str();
    }
};

class BooleanLiteral : public Expression {
  public:
    BooleanLiteral(Token t, bool val) 
      : Expression(NodeType::BOOLEAN_LITERAL), token(t), value(val) {}

    Token token;
    bool value;

    void expression_node() {}
    auto token_literal() -> std::string { return token.literal; }
    auto to_string() -> std::string { return token.literal; }
};

class IfExpression : public Expression {
  public:
    IfExpression(Token t) 
      : Expression(NodeType::IF_EXPRESSION), token(t), condition(nullptr), 
      consequence(nullptr), alternative(nullptr) {}

    Token token;
    Expression *condition;
    BlockStatement *consequence;
    BlockStatement *alternative;

    void expression_node() {}
    auto token_literal() -> std::string { return token.literal; }
    auto to_string() -> std::string { 
      std::stringstream tmp_s;
      tmp_s << "if (" << condition->to_string() << ") " << consequence->to_string();
      if (alternative != nullptr) {
        tmp_s << "else " << alternative->to_string();
      }
      return tmp_s.str();
    }
};

class FunctionLiteral : public Expression {
  public:
    FunctionLiteral(Token t) 
      : Expression(NodeType::FUNCTION_LITERAL), token(t) {}

    Token token;
    std::vector<Identifier *> parameters;
    BlockStatement *body;

    void expression_node() {}
    auto token_literal() -> std::string { return token.literal; }
    auto to_string() -> std::string {
      std::stringstream tmp_s;

      tmp_s << token.literal;

      tmp_s << "(";
      for(size_t i = 0; i < parameters.size(); ++i) {
        tmp_s << parameters[i]->to_string();
        tmp_s << (i < (parameters.size()-1) ? ", " : "");
      }
      tmp_s << ")";

      tmp_s << body->to_string();

      return tmp_s.str();
    }
};

class CallExpression : public Expression {
  public:
    CallExpression(Token t, Expression *fn) 
      : Expression(NodeType::CALL_EXPRESSION), token(t), function(fn) {}

    void expression_node() {}
    auto token_literal() -> std::string { return token.literal; }
    auto to_string() -> std::string {
      std::stringstream tmp_s;

      tmp_s << function->to_string();
      tmp_s << "(";
      for(size_t i = 0; i < arguments.size(); ++i) {
        tmp_s << arguments[i]->to_string();
        tmp_s << (i < (arguments.size()-1) ? ", " : "");
      }
      tmp_s << ")";

      return tmp_s.str();
    }

    Token token;
    Expression *function;
    std::vector<Expression *> arguments;
};

namespace Ast {
  struct Program {
    std::vector<Statement *> statements;

    auto to_string() -> std::string {
      std::stringstream tmp_s;
      for(const auto &s : statements) {
        tmp_s << s->to_string();
      }

      return tmp_s.str();
    }
  };

  auto type2str(NodeType nt) -> std::string;

}// namespace Ast
