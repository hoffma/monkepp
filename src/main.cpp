#include <iostream>
#include <fstream>
#include <sstream>
#include <string_view>

#include "token.h"
#include "lexer.h"
#include "parser.h"
#include "evaluator.h"

using namespace std;

constexpr string_view PROMPT = ">> ";

void print_parser_erros(Parser &p) {
  auto errors = p.get_errors();
  if (errors.size() == 0)
    return;

  std::cerr << "Parser has " << errors.size() << " errors." << std::endl;
  for (const auto &e : errors) {
    std::cout << "parser error: " << e << std::endl;
  }
} // print_parser_erros();

std::string read_file(std::string fname) {
  std::string content{""};

  std::ifstream in_file(fname);
  if(in_file.is_open()) {
    std::stringstream buf;
    buf << in_file.rdbuf();
    content = buf.str();
    in_file.close();
  }

  return content;
}

void run(std::string &source) {
  Environment env;

  Lexer l{source};
  Parser p{l};
  auto program = p.parse_program();
  if (program.has_value()) {
    auto evaluated = evaluator::eval(&program.value(), &env);
    if (evaluated)
      cout << evaluated->inspect() << endl;
  }
  cout << "\nBye!" << std::endl;
}

void start_repl() {
  Environment env;

  while(1) {
    string in;

    cout << PROMPT << flush;
    getline(cin, in);

    if (in == "exit" or in == "quit") {
      cout << "Quitting. Bye." << endl;
      break;
    }

    Lexer l{in};
    Parser p{l};

    auto program = p.parse_program();
    if (p.get_errors().size()) {
      print_parser_erros(p);
      continue;
    }

    if (program.has_value()) {
      auto evaluated = evaluator::eval(&program.value(), &env);
      if (evaluated)
        cout << evaluated->inspect() << endl;
    } else {
      cerr << "Program is empty!" << endl;
    }
  }
}

int main(int argc, char *argv[]) {
  std::cout << "Welcome to monkePP." << std::endl; 


  /* TODO: read file if input file is given */
  if (argc > 1) {
    std::string src = read_file(argv[1]);
    run(src);
  } else {
    std::cout << "Starting the REPL. Please type monke" << std::endl;
    start_repl();
  }


  return 0;
}
