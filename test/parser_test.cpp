#include <catch2/catch_all.hpp>
#include <catch2/catch_test_macros.hpp>
#include <optional>
#include <string_view>

#include "ast.h"
#include "lexer.h"
#include "parser.h"
#include "type.h"

bool test_let_statement(Statement *s, const std::string &name) {
  LetStatement *ls = dynamic_cast<LetStatement *>(s);
  if (ls == nullptr) {
    std::cerr << "Error casting " << s->to_string() << " to LetStatement"
              << std::endl;
    return false;
  }

  if (ls->token_literal() != "let") {
    std::cerr << "ls.token_literal() not 'let'. Got: " << ls->token_literal()
              << std::endl;
    return false;
  }

  if (ls->name->value != name) {
    INFO("ls->name not '" << name << "'");
    return false;
  }

  if (ls->name->token_literal() != name) {
    std::cerr << "ls->name.token_literal() not '" << name << "'"
              << ". Got: " << ls->name->token_literal() << std::endl;
    return false;
  }

  return true;
} // test_let_statement();

bool test_identifier(Expression *e, const std::string &value) {
  Identifier *ident = dynamic_cast<Identifier *>(e);
  if (ident == nullptr) {
    std::cerr << "Error casting " << e->to_string() << " to Identifier"
              << std::endl;
    return false;
  }

  if (ident->value != value) {
    std::cerr << "ident->value not " << value << ", got: " << ident->value
              << std::endl;
    return false;
  }

  if (ident->token_literal() != value) {
    std::cerr << "ident->token_literal() not " << value
              << ", got: " << ident->token_literal() << std::endl;
    return false;
  }

  return true;
}

auto test_integer_literal(Expression *il, int64_t value) -> bool {
  auto *intg = dynamic_cast<IntegerLiteral *>(il);

  if (intg == nullptr) {
    std::cerr << "Error casting " << il->to_string() << " to IntegerLiteral"
              << ", got: " << il->to_string() << std::endl;
    return false;
  }

  if (intg->value != value) {
    std::cerr << "intg.value not " << value << ", got: " << intg->value
              << std::endl;
    return false;
  }

  return true;
}

template <typename T>
bool test_literal_expression(Expression *exp, T expected) {
  if (exp == nullptr)
    return false;

  if constexpr (std::is_same<T, bool>::value) {
    return test_boolean_literal(exp, expected);
  }
  if constexpr (std::is_same<T, int>::value) {
    return test_integer_literal(exp, expected);
  }
  if constexpr (std::is_same<T, int64_t>::value) {
    return test_integer_literal(exp, expected);
  }
  if constexpr (std::is_same<T, std::string>::value) {
    return test_identifier(exp, expected);
  }
  /*INFO("Type of " << typeid(T).name() << " not handled");*/
  INFO("Type of " << type(expected) << " not handled");
  return false;
}

template <typename T, typename U>
bool test_infix_expression(Expression *exp, T left, std::string op, U right) {
  InfixExpression *opExp = dynamic_cast<InfixExpression *>(exp);

  if (opExp == nullptr) {
    std::cerr << "Error casting " << exp->to_string() << " to InfixExpression"
              << std::endl;
    return false;
  }

  if (!test_literal_expression(opExp->left, left)) {
    return false;
  }

  if (opExp->op != op) {
    INFO("opExp->op != op: " << opExp->op << " != " << op);
    return false;
  }

  if (!test_literal_expression(opExp->right, right)) {
    return false;
  }

  return true;
}

bool test_boolean_literal(Expression *exp, bool value) {
  auto *b = dynamic_cast<BooleanLiteral *>(exp);

  if (b == nullptr) {
    INFO("b is not a BooleanLiteral. Got: " << exp->to_string());
  }

  if (b->value != value) {
    INFO("b->value not " << value << ", got: " << b->value);
  }

  if (b->token_literal() != (value ? "true" : "false")) {
    std::cerr << "b->token_literal() not " << value << ", got: " 
      << b->token_literal() << std::endl;
  }

  return true;
}

void check_parser_errors(Parser &p) {
  auto errors = p.get_errors();
  if (errors.size() == 0)
    return;

  INFO("Parser has " << errors.size() << " errors.");
  for (const auto &e : errors) {
    std::cout << "parser error: " << e << std::endl;
  }

  REQUIRE(false);
} // check_parser_errors();

TEST_CASE("Test let Statements", "[parser]") {
  INFO("### Test let Statements ###");
  std::string_view input{R"(
    let x = 5;
    let y = 10;
    let foobar = 838383;
    )"};

  Lexer l{std::string{input}};
  Parser p{l};

  auto program = p.parse_program();
  if (!program.has_value()) {
    INFO("parse_program() returned nothing");
    exit(EXIT_FAILURE);
  }
  check_parser_errors(p);

  if (program.value().statements.size() != 3) {
    std::cerr << "program.statements does not contain 3 statements. Got: "
              << program.value().statements.size() << std::endl;
  }

  std::vector<std::string> tests{"x", "y", "foobar"};

  for (size_t i = 0; i < tests.size(); ++i) {
    auto tt = tests[i];
    auto *stmt = program->statements[i];
    REQUIRE(test_let_statement(stmt, tt));
    /*if (!test_let_statement(*stmt, tt)) {
    }*/
  }

  for (auto &tmp : program->statements) {
    delete tmp;
  }
}

TEST_CASE("Test return statements", "[parser]") {
  INFO("### Test Return Statements ###");
  std::string_view input{R"(
    return 5;
    return 10;
    return 993322;
  )"};

  Lexer l{std::string{input}};
  Parser p{l};

  auto program = p.parse_program();
  check_parser_errors(p);
  if (!program.has_value()) {
    INFO("parse_program() returned nothing");
    REQUIRE(false);
  }

  if (program.value().statements.size() != 3) {
    std::cerr << "program.statements does not contain 3 statements. Got: "
              << program.value().statements.size() << std::endl;
    REQUIRE(false);
  }

  for (const auto &stmt : program->statements) {
    if (stmt->token_literal() != "return") {
      std::cerr << "token_literal not 'return', got: " << stmt->token_literal()
                << std::endl;
      REQUIRE(false);
    }
  }
}

TEST_CASE("Test ast", "[parser]") {
  INFO("### Test Ast ###");
  Ast::Program p{std::vector<Statement *>{
      new LetStatement{
          Token{TokenType::LET, "let"},
          new Identifier{Token{TokenType::IDENT, "myVar"}, "myVar"},
          new Identifier{Token{TokenType::IDENT, "anotherVar"}, "anotherVar"}},
  }};
  REQUIRE(p.to_string() == "let myVar = anotherVar;");
}

TEST_CASE("Test Identifier Expression", "[parser]") {
  INFO("### Test Identifier Expressions ###");
  std::string_view input{"foobar;"};

  Lexer l{std::string{input}};
  Parser p{l};

  auto program = p.parse_program();
  if (!program.has_value()) {
    INFO("parse_program() returned nothing");
    REQUIRE(false);
  }
  check_parser_errors(p);

  if (program.value().statements.size() != 1) {
    std::cerr << "program.statements does not contain 1 statement. Got: "
              << program.value().statements.size() << std::endl;
    REQUIRE(false);
  }

  auto *stmt = program->statements[0];
  ExpressionStatement *es = dynamic_cast<ExpressionStatement *>(stmt);
  auto ident = es->expr;
  REQUIRE(ident->to_string() == "foobar");
  REQUIRE(ident->token_literal() == "foobar");
  /*auto ident = stmt.expr;*/
}

TEST_CASE("Test Integer Literal Expression", "[parser]") {
  INFO("### Test Integer Literal Expressions ###");
  std::string_view input{"5;"};
  Lexer l{std::string{input}};
  Parser p{l};

  auto program = p.parse_program();
  if (!program.has_value()) {
    INFO("parse_program() returned nothing");
    REQUIRE(false);
  }
  check_parser_errors(p);

  if (program.value().statements.size() != 1) {
    std::cerr << "program.statements does not contain 1 statement. Got: "
              << program.value().statements.size() << std::endl;
    REQUIRE(false);
  }

  auto *stmt = program->statements[0];
  auto *es = dynamic_cast<ExpressionStatement *>(stmt);
  auto *literal = dynamic_cast<IntegerLiteral *>(es->expr);
  REQUIRE(literal->value == 5);
  REQUIRE(literal->token_literal() == "5");
}

TEST_CASE("Test String Literal Expression", "[parser]") {
  INFO("### Test String Literal Expressions ###");
  std::string_view input{"\"hello world\";"};
  Lexer l{std::string{input}};
  Parser p{l};

  auto program = p.parse_program();
  if (!program.has_value()) {
    INFO("parse_program() returned nothing");
    REQUIRE(false);
  }
  check_parser_errors(p);

  if (program.value().statements.size() != 1) {
    std::cerr << "program.statements does not contain 1 statement. Got: "
              << program.value().statements.size() << std::endl;
    REQUIRE(false);
  }

  auto *stmt = program->statements[0];
  auto *es = dynamic_cast<ExpressionStatement *>(stmt);
  auto *literal = dynamic_cast<StringLiteral *>(es->expr);
  REQUIRE(literal->value == "hello world");
  REQUIRE(literal->token_literal() == "hello world");
}

TEST_CASE("Test BooleanLiteral Expression", "[parser]") {
  INFO("### Test BooleanLiteral Expressions ###");
  std::string_view input{"false;"};

  Lexer l{std::string{input}};
  Parser p{l};

  auto program = p.parse_program();
  if (!program.has_value()) {
    INFO("parse_program() returned nothing");
    REQUIRE(false);
  }
  check_parser_errors(p);

  if (program.value().statements.size() != 1) {
    std::cerr << "program.statements does not contain 1 statement. Got: "
              << program.value().statements.size() << std::endl;
    REQUIRE(false);
  }

  auto *stmt = program->statements[0];
  ExpressionStatement *es = dynamic_cast<ExpressionStatement *>(stmt);
  auto ident = es->expr;
  REQUIRE(ident->to_string() == "false");
  REQUIRE(ident->token_literal() == "false");
  /*auto ident = stmt.expr;*/
}

TEST_CASE("Test Parsing Integer Prefix Expressions", "[parser]") {
  INFO("### Test Integer Prefix Expressions ###");
  struct PrefixTests {
    std::string input;
    std::string op;
    int64_t integerValue;
  };

  PrefixTests tests[]{{"!5;", "!", 5}, {"-15;", "-", 15}};

  for (auto &test : tests) {
    Lexer l{std::string{test.input}};
    Parser p{l};

    auto program = p.parse_program();
    if (!program.has_value()) {
      INFO("parse_program() returned nothing");
      REQUIRE(false);
    }
    check_parser_errors(p);

    if (program.value().statements.size() != 1) {
      std::cerr << "program.statements does not contain 1 statement. Got: "
                << program.value().statements.size() << std::endl;
      REQUIRE(false);
    }

    auto *stmt = dynamic_cast<ExpressionStatement *>(program->statements[0]);
    auto *expr = dynamic_cast<PrefixExpression *>(stmt->expr);
    REQUIRE(expr->op == test.op);
    REQUIRE(test_integer_literal(expr->right, test.integerValue) == true);
  }
}

TEST_CASE("Test Parsing BooleanLiteral Prefix Expressions", "[parser]") {
  INFO("### Test BooleanLiteral Prefix Expressions ###");
  struct PrefixTests {
    std::string input;
    std::string op;
    bool boolValue;
  };

  PrefixTests tests[]{{"!true;", "!", true}, {"!false;", "!", false}};

  for (auto &test : tests) {
    Lexer l{std::string{test.input}};
    Parser p{l};

    auto program = p.parse_program();
    if (!program.has_value()) {
      INFO("parse_program() returned nothing");
      REQUIRE(false);
    }
    check_parser_errors(p);

    if (program.value().statements.size() != 1) {
      std::cerr << "program.statements does not contain 1 statement. Got: "
                << program.value().statements.size() << std::endl;
      REQUIRE(false);
    }

    auto *stmt = dynamic_cast<ExpressionStatement *>(program->statements[0]);
    auto *expr = dynamic_cast<PrefixExpression *>(stmt->expr);
    REQUIRE(expr->op == test.op);
    REQUIRE(test_literal_expression(expr->right, test.boolValue) == true);
  }
}

TEST_CASE("Test Parsing Infix Integer Expressions", "[parser]") {
  INFO("### Test Parsing Infix Integer Expressions ###");
  struct InfixTests {
    std::string input;
    int64_t leftValue;
    std::string op;
    int64_t rightValue;
  };

  InfixTests tests[]{
      {"5 + 5", 5, "+", 5},   {"5 - 5", 5, "-", 5},   {"5 * 5", 5, "*", 5},
      {"5 / 5", 5, "/", 5},   {"5 < 5", 5, "<", 5},   {"5 > 5", 5, ">", 5},
      {"5 == 5", 5, "==", 5}, {"5 != 5", 5, "!=", 5},
  };

  for (auto &test : tests) {
    Lexer l{std::string{test.input}};
    Parser p{l};

    auto program = p.parse_program();
    if (!program.has_value()) {
      INFO("parse_program() returned nothing");
      REQUIRE(false);
    }
    check_parser_errors(p);

    if (program.value().statements.size() != 1) {
      std::cerr << "program.statements does not contain 1 statement. Got: "
                << program.value().statements.size() << std::endl;
      REQUIRE(false);
    }

    auto *stmt = dynamic_cast<ExpressionStatement *>(program->statements[0]);
    REQUIRE(test_infix_expression(stmt->expr, test.leftValue, test.op, test.rightValue));
  }
}

TEST_CASE("Test Parsing Infix BooleanLiteral Expressions", "[parser]") {
  INFO("### Test Parsing Infix BooleanLiteral Expressions ###");
  struct InfixTests {
    std::string input;
    bool leftValue;
    std::string op;
    bool rightValue;
  };

  InfixTests tests[]{
      {"true == true", true, "==", true},
      {"true != false", true, "!=", false},
      {"false == false", false, "==", false},
  };

  for (auto &test : tests) {
    Lexer l{std::string{test.input}};
    Parser p{l};

    auto program = p.parse_program();
    if (!program.has_value()) {
      INFO("parse_program() returned nothing");
      REQUIRE(false);
    }
    check_parser_errors(p);

    if (program.value().statements.size() != 1) {
      std::cerr << "program.statements does not contain 1 statement. Got: "
                << program.value().statements.size() << std::endl;
      REQUIRE(false);
    }

    auto *stmt = dynamic_cast<ExpressionStatement *>(program->statements[0]);
    REQUIRE(test_infix_expression(stmt->expr, test.leftValue, test.op, test.rightValue));
  }
}

TEST_CASE("Test Operator Precedence Parsing", "[parser]") {
  INFO("### Test Operator Precedence Parsing ###");
  struct TestType {
    std::string input;
    std::string expected;
  };

  TestType tests[]{
      {"-a*b", "((-a) * b)"},
      {"!-a", "(!(-a))"},
      {"a + b - c", "((a + b) - c)"},
      {"a * b*c", "((a * b) * c)"},
      {"a*b/c", "((a * b) / c)"},
      {"a+b/c", "(a + (b / c))"},
      {"a+b*c+d/e-f", "(((a + (b * c)) + (d / e)) - f)"},
      {"3+4; -5*5", "(3 + 4)((-5) * 5)"},
      {"5 > 4 == 3 < 4", "((5 > 4) == (3 < 4))"},
      {"5 < 4 != 3 > 4", "((5 < 4) != (3 > 4))"},
      {"3 + 4 * 5 == 3 * 1 + 4 * 5", "((3 + (4 * 5)) == ((3 * 1) + (4 * 5)))"},
      {"true", "true"},
      {"false", "false"},
      {"3 > 5 == false", "((3 > 5) == false)"},
      {"3 < 5 == true", "((3 < 5) == true)"},
      {"1 + (2 + 3) + 4", "((1 + (2 + 3)) + 4)"},
      {"(5 + 5) * 2", "((5 + 5) * 2)"},
      {"2 / (5 + 5)", "(2 / (5 + 5))"},
      {"-(5 + 5)", "(-(5 + 5))"},
      {"!(true == true)", "(!(true == true))"},
      {"a + add(b * c) + d", "((a + add((b * c))) + d)"},
      {"add(a, b, 1, 2 * 3, 4 +5, add(6, 7 * 8))", "add(a, b, 1, (2 * 3), (4 + 5), add(6, (7 * 8)))"},
      {"add(a + b + c * d / f + g)", "add((((a + b) + ((c * d) / f)) + g))"},
  };

  for (auto &test : tests) {
    Lexer l{std::string{test.input}};
    Parser p{l};

    auto program = p.parse_program();
    if (!program.has_value()) {
      INFO("parse_program() returned nothing");
      REQUIRE(false);
    }
    check_parser_errors(p);

    std::string actual = program->to_string();

    REQUIRE(actual == test.expected);
  }
}

TEST_CASE("Test If-Expression", "[parser]") {
  INFO("### Test If-Expression ###");
  std::string input{"if (x < y) { x }"};

  Lexer l{input};
  Parser p{l};
  auto program = p.parse_program();
  if (!program.has_value()) {
    INFO("parse_program() returned nothing");
    REQUIRE(false);
  }
  check_parser_errors(p);

  if (program.value().statements.size() != 1) {
    std::cerr << "program.statements does not contain 1 statement. Got: "
              << program.value().statements.size() << std::endl;
    for (auto &stmt : program.value().statements) {
      INFO(stmt->to_string());
    }
    REQUIRE(false);
  }

  auto *stmt = dynamic_cast<ExpressionStatement *>(program->statements[0]);

  if (stmt == nullptr) {
    std::cerr << "statements[0] is not an ExpressionStatement, got: "
      << program->statements[0]->to_string() << std::endl;
    REQUIRE(false);
  }

  auto *expr = dynamic_cast<IfExpression *>(stmt->expr);
  if (expr == nullptr) {
    INFO("Cannot cast " << expr->to_string() << "to IfExpression");
    REQUIRE(false);
  }

  std::string x{"x"};
  std::string y{"y"};
  REQUIRE(test_infix_expression(expr->condition, x, "<", y));

  if (expr->consequence->statements.size() != 1) {
    std::cerr << "Consequence is not 1 statement. Got: "
      << expr->consequence->statements.size() << std::endl;
    REQUIRE(false);
  }

  auto *conseq_stmt = dynamic_cast<ExpressionStatement *>(expr->consequence->statements[0]);
  if (conseq_stmt == nullptr) {
    std::cerr << "Consequence is not an ExpressionStatement. Got: "
      << conseq_stmt->to_string() << std::endl;
  }

  REQUIRE(test_identifier(conseq_stmt->expr, "x"));

  REQUIRE((expr->alternative == nullptr));
}

TEST_CASE("Test If-Else-Expression", "[parser]") {
  INFO("### Test If-Else-Expression ###");
  std::string input{"if (x < y) { x } else { y }"};

  Lexer l{input};
  Parser p{l};
  auto program = p.parse_program();
  if (!program.has_value()) {
    INFO("parse_program() returned nothing");
    REQUIRE(false);
  }
  check_parser_errors(p);

  if (program.value().statements.size() != 1) {
    std::cerr << "program.statements does not contain 1 statement. Got: "
              << program.value().statements.size() << std::endl;
    for (auto &stmt : program.value().statements) {
      INFO(stmt->to_string());
    }
    REQUIRE(false);
  }

  auto *stmt = dynamic_cast<ExpressionStatement *>(program->statements[0]);

  if (stmt == nullptr) {
    std::cerr << "statements[0] is not an ExpressionStatement, got: "
      << program->statements[0]->to_string() << std::endl;
    REQUIRE(false);
  }

  auto *expr = dynamic_cast<IfExpression *>(stmt->expr);
  if (expr == nullptr) {
    INFO("Cannot cast " << expr->to_string() << "to IfExpression");
    REQUIRE(false);
  }

  std::string x{"x"};
  std::string y{"y"};
  REQUIRE(test_infix_expression(expr->condition, 
        x,  "<", y));

  if (expr->consequence->statements.size() != 1) {
    std::cerr << "Consequence is not 1 statement. Got: "
      << expr->consequence->statements.size() << std::endl;
    REQUIRE(false);
  }

  auto *conseq_stmt = dynamic_cast<ExpressionStatement *>(expr->consequence->statements[0]);
  if (conseq_stmt == nullptr) {
    std::cerr << "Consequence is not an ExpressionStatement. Got: "
      << conseq_stmt->to_string() << std::endl;
  }

  REQUIRE(test_identifier(conseq_stmt->expr, "x"));

  /* alternative path */
  if (expr->alternative->statements.size() != 1) {
    std::cerr << "Alternative is not 1 statement. Got: "
      << expr->alternative->statements.size() << std::endl;
    REQUIRE(false);
  }

  auto *alt_stmt = dynamic_cast<ExpressionStatement *>(expr->alternative->statements[0]);
  if (alt_stmt == nullptr) {
    std::cerr << "Alternative is not an ExpressionStatement. Got: "
      << alt_stmt->to_string() << std::endl;
  }
  REQUIRE(test_identifier(alt_stmt->expr, "y"));
}

TEST_CASE("Test FunctionLiteral Parsing", "[parser]") {
  INFO("### Test FunctionLiteral Parsing ###");

  std::string input{"fn(x, y) { x + y; }"};

  Lexer l{input};
  Parser p{l};
  auto program = p.parse_program();
  if (!program.has_value()) {
    INFO("parse_program() returned nothing");
    REQUIRE(false);
  }
  check_parser_errors(p);

  if (program.value().statements.size() != 1) {
    std::cerr << "program.statements does not contain 1 statement. Got: "
              << program.value().statements.size() << std::endl;
    for (auto &stmt : program.value().statements) {
      INFO(stmt->to_string());
    }
    REQUIRE(false);
  }

  auto *stmt = dynamic_cast<ExpressionStatement *>(program->statements[0]);

  if (stmt == nullptr) {
    std::cerr << "statements[0] is not an ExpressionStatement, got: "
      << program->statements[0]->to_string() << std::endl;
    REQUIRE(false);
  }

  auto *fn = dynamic_cast<FunctionLiteral *>(stmt->expr);
  REQUIRE((fn != nullptr));
  REQUIRE((fn->parameters.size() == 2));
  test_literal_expression(fn->parameters[0], std::string{"x"});
  test_literal_expression(fn->parameters[1], std::string{"y"});

  REQUIRE((fn->body->statements.size() == 1));

  auto *body_stmt = dynamic_cast<ExpressionStatement *>(fn->body->statements[0]);
  REQUIRE((body_stmt != nullptr));

  test_infix_expression(body_stmt->expr, std::string{"x"}, "+", std::string{"y"});
}

TEST_CASE("Test Function Parameter Parsing", "[parser]") {
  INFO("### Test Function Parameter Parsing ###");
  struct TestCase {
    std::string input;
    std::vector<std::string> expected;
  };

  std::vector<TestCase> tests{
    {"fn() {}", {}},
    {"fn(x) {}", {"x"}},
    {"fn(x, y) {}", {"x", "y"}}
  };

  for (auto &test : tests) {
    Lexer l{test.input};
    Parser p{l};
    auto program = p.parse_program();
    if (!program.has_value()) {
      INFO("parse_program() returned nothing");
      REQUIRE(false);
    }
    check_parser_errors(p);

    auto *stmt = dynamic_cast<ExpressionStatement *>(program->statements[0]);
    REQUIRE((stmt != nullptr));
    auto *fn = dynamic_cast<FunctionLiteral *>(stmt->expr);
    REQUIRE((fn != nullptr));

    if (fn->parameters.size() != test.expected.size()) {
      std::cerr << "function parameter list does not contain " 
        << test.expected.size() << " parameters. Got: "
        << fn->parameters.size() << std::endl;
      for (auto &st : program.value().statements) {
        INFO(st->to_string());
      }
      REQUIRE(false);
    }

    /*for (auto &ident : test.expected) {*/
    for (size_t i = 0; i < test.expected.size(); ++i) {
      test_literal_expression(fn->parameters[i], test.expected[i]);
    }
  }
}

TEST_CASE("Test Call Expression Parsing", "[parser]") {
  INFO("### Test Call Expression Parsing ###");

  std::string input{"add(1, 2 * 3, 4 + 5);"};

  Lexer l{input};
  Parser p{l};
  auto program = p.parse_program();
  if (!program.has_value()) {
    INFO("parse_program() returned nothing");
    REQUIRE(false);
  }
  check_parser_errors(p);

  if (program.value().statements.size() != 1) {
    std::cerr << "program.statements does not contain 1 statement. Got: "
              << program.value().statements.size() << std::endl;
    for (auto &stmt : program.value().statements) {
      INFO(stmt->to_string());
    }
    REQUIRE(false);
  }

  auto *stmt = dynamic_cast<ExpressionStatement *>(program->statements[0]);
  REQUIRE((stmt != nullptr));

  auto *expr = dynamic_cast<CallExpression *>(stmt->expr);
  REQUIRE((expr != nullptr));

  REQUIRE(test_identifier(expr->function, "add"));

  REQUIRE(expr->arguments.size() == 3);

  test_literal_expression(expr->arguments[0], 1);
  test_infix_expression(expr->arguments[1], 2, "*", 3);
  test_infix_expression(expr->arguments[2], 4, "+", 5);
}

template <typename T>
struct TestCase {
  std::string input;
  std::string expectedIdent;
  T expectedVal;

  TestCase(std::string i, std::string ei, T ev) 
    : input(i), expectedIdent(ei), expectedVal(ev) {}
};

template <typename T>
bool test_let_helper(std::string input, std::string expectedIdent, T expectedVal) {
  Lexer l{input};
  Parser p{l};
  auto program = p.parse_program();
  if (!program.has_value()) {
    INFO("parse_program() returned nothing");
    return false;
  }
  check_parser_errors(p);

  if (program.value().statements.size() != 1) {
    std::cerr << "program.statements does not contain 1 statement. Got: "
              << program.value().statements.size() << std::endl;
    for (auto &stmt : program.value().statements) {
      INFO(stmt->to_string());
    }
    return false;
  }

  auto *stmt = program->statements[0];
  INFO("stmt: " << stmt->to_string());
  REQUIRE(test_let_statement(stmt, expectedIdent));

  auto *val = dynamic_cast<LetStatement *>(stmt)->value;
  INFO("val: " << val->to_string());
  REQUIRE((val != nullptr));
  REQUIRE(test_literal_expression(val, expectedVal));

  return true;
}

TEST_CASE("Test Let Statements", "[parser]") {
  INFO("### Test Let Statements ###");

  TestCase t1{"let x = 5;", "x", 5};
  TestCase t2{"let y = true;", "y", true};
  TestCase t3{"let foobar = y;", "foobar", std::string{"y"}};

  REQUIRE(test_let_helper(t1.input, t1.expectedIdent, t1.expectedVal));
  REQUIRE(test_let_helper(t2.input, t2.expectedIdent, t2.expectedVal));
  REQUIRE(test_let_helper(t3.input, t3.expectedIdent, t3.expectedVal));
}

