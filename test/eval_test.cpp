#include <catch2/catch_all.hpp>
#include <catch2/catch_test_macros.hpp>

#include "evaluator.h"
#include "object.h"
#include "parser.h"
#include "type.h"

Object *test_eval(std::string input) {
  Lexer l{input};
  Parser p{l};
  auto tmp = p.parse_program();
  REQUIRE(tmp.has_value());
  auto program = tmp.value();
  auto env = Environment();

  return evaluator::eval(&program, &env);
}

bool test_integer_obj(Object *obj, int64_t expected) {
  auto *io = dynamic_cast<IntegerObj *>(obj);
  if (io == nullptr)
    return false;

  if (io->value != expected) {
    std::cerr << "Object has wrong value. Got " << io->value
              << ", but expected " << expected << std::endl;
    return false;
  }
  return true;
}

bool test_null_obj(Object *obj) {
  if (!evaluator::is_null(obj)) {
    std::cerr << "Object is not Null. Got " << obj->inspect() << std::endl;
    return false;
  }

  return true;
}

bool test_bool_obj(Object *obj, bool expected) {
  auto *io = dynamic_cast<BooleanObj *>(obj);
  if (io == nullptr) {
    std::cout << "hello nullptr" << std::endl;
    return false;
  }
  if (io->value != expected) {
    std::cerr << "Object has wrong value. Got " << io->value
              << ", but expected " << expected << std::endl;
    return false;
  }
  return true;
}

TEST_CASE("Test Eval Integer Expression", "[evaluator]") {
  INFO("### Test Eval Integer Expression ###");

  struct TestType {
    std::string input;
    int64_t expected;
  };

  TestType cases[] = {
      {"5", 5},
      {"10", 10},
      {"-5", -5},
      {"-10", -10},
      {"2 * 2", 4},
      {"5 + 5 + 5 + 5 - 10", 10},
      {"2 * 2 * 2 * 2 * 2", 32},
      {"-50 + 100 + -50", 0},
      {"5 * 2 + 10", 20},
      {"5 + 2 * 10", 25},
      {"20 + 2 * -10", 0},
      {"50 / 2 * 2 + 10", 60},
      {"2 * (5 + 10)", 30},
      {"3 * 3 * 3 + 10", 37},
      {"3 * (3 * 3) + 10", 37},
      {"(5 + 10 * 2 + 15 / 3) * 2 + -10", 50},
  };

  for (auto &test : cases) {
    auto evaluated = test_eval(test.input);
    INFO("evaluated: " << evaluated->inspect()
                       << ", expected: " << test.expected);
    REQUIRE(evaluated);
    REQUIRE(test_integer_obj(evaluated, test.expected));
  }
}

TEST_CASE("Test Eval Bool Expression", "[evaluator]") {
  INFO("### Test Eval Bool Expression ###");

  struct TestType {
    std::string input;
    bool expected;
  };

  TestType cases[] = {
      {"true", true},
      {"false", false},
      {"1 < 2", true},
      {"1 > 2", false},
      {"1 < 1", false},
      {"2 > 2", false},
      {"1 == 1", true},
      {"2 != 2", false},
      {"1 == 2", false},
      {"1 != 2", true},
      {"true == true", true},
      {"false == false", true},
      {"true == false", false},
      {"true != true", false},
      {"false != true", true},
      {"(1 < 2) == true", true},
      {"(1 < 2) == false", false},
      {"(1 > 2) == true", false},
      {"(1 > 2) == false", true},
  };

  for (auto &test : cases) {
    auto evaluated = test_eval(test.input);
    INFO("Case input: " << test.input << ", expected: " << test.expected);
    INFO("evaluated: " << evaluated->inspect()
                       << ", expected: " << test.expected);
    REQUIRE(evaluated);
    REQUIRE(test_bool_obj(evaluated, test.expected));
  }
}

TEST_CASE("Test Eval Bang Operator", "[evaluator]") {
  INFO("### Test Eval Bang Operator ###");

  struct TestType {
    std::string input;
    bool expected;
  };

  TestType cases[] = {
      {"!true", false}, {"!false", true},   {"!5", false},
      {"!!true", true}, {"!!false", false}, {"!!5", true},
  };

  for (auto &test : cases) {
    auto evaluated = test_eval(test.input);
    INFO("evaluated: " << evaluated->inspect()
                       << ", expected: " << test.expected);
    REQUIRE(evaluated);
    REQUIRE(test_bool_obj(evaluated, test.expected) == true);
  }
}

TEST_CASE("Test If-Else Expressions", "[evaluator]") {
  INFO("### Test If-Else Expressions ###");

  struct TestType {
    std::string input;
    int64_t expected;
    bool is_null;
  };

  TestType tests[] {
    {"if (true) { 10 }", 10, false},
    {"if (false) { 10 }", -1, true},
    {"if (1) { 10 }", 10, false},
    {"if (1 < 2) { 10 }", 10, false},
    {"if (1 > 2) { 10 }", -1, true},
    {"if (1 > 2) { 10 } else { 20 }", 20, false},
    {"if (1 < 2) { 10 } else { 20 }", 10, false},
  };

  for (auto &test : tests) {
    auto *evaluated = test_eval(test.input);
    INFO("Case input: " << test.input << ", expected: " << test.expected);
    INFO("evaluated: " << evaluated->inspect());
    REQUIRE(evaluated);
    if (!test.is_null) {
      INFO("IS_NULL == false");
      REQUIRE(test_integer_obj(evaluated, test.expected));
    } else {
      INFO("IS_NULL == true");
      REQUIRE(test_null_obj(evaluated));
    }
  }
}

TEST_CASE("Test Return Statements", "[evaluator]") {
  INFO("### Test Return Statements ###");

  struct TestType {
    std::string input;
    int64_t expected;
  };

  std::string tmp_input{R"(
    if (10 > 1) {
      if (10 > 1) {
        return 10;
      }

      return 1;
    }
  )"};

  TestType tests[] {
    {"return 10;", 10},
    {"return 10; 9;", 10},
    {"return 2*5; 9;", 10},
    {"9; return 2*5; 9;", 10},
    {tmp_input, 10},
  };

  for (auto &test : tests) {
    auto *evaluated = test_eval(test.input);
    INFO("Case input: " << test.input << ", expected: " << test.expected);
    INFO("evaluated: " << evaluated->inspect());
    REQUIRE(evaluated);
    REQUIRE(test_integer_obj(evaluated, test.expected));
  }
}

TEST_CASE("Test Error Handling", "[evaluator]") {
  INFO("### Test Error Handling ###");

  struct TestType {
    std::string input;
    std::string expected;
  };

  TestType tests[] {
    {"5 + true;", "type mismatch: INTEGER + BOOLEAN"},
    {"5 + true; 5;", "type mismatch: INTEGER + BOOLEAN"},
    {"-true", "unknown operator: -BOOLEAN"},
    {"true + false;", "unknown operator: BOOLEAN + BOOLEAN"},
    {"5; true + false; 5;", "unknown operator: BOOLEAN + BOOLEAN"},
    {"if (10 > 1) { true + false; }", "unknown operator: BOOLEAN + BOOLEAN"},
    {"foobar", "identifier not found: foobar"},
    {"\"Hello\" - \"World\"", "unknown operator: STRING - STRING"},
  };

  for (auto &test : tests) {
    auto *evaluated = test_eval(test.input);
    auto *err_obj = dynamic_cast<ErrorObj *>(evaluated);
    INFO("Case input: " << test.input << "\n\texpected:\t" << test.expected);
    INFO("\tevaluated:\t" << err_obj->message);
    REQUIRE(evaluated);
    REQUIRE(err_obj);

    REQUIRE(err_obj->message == test.expected);
  }
}

TEST_CASE("Test Let Statement", "[evaluator]") {
  INFO("### Test Let Statement ###");

  struct TestType {
    std::string input;
    int64_t expected;
  };

  TestType tests[] {
    {"let a = 5; a;", 5},
    {"let a = 5 * 5; a;", 25},
    {"let a = 5; let b = a; b;", 5},
    {"let a = 5; let b = a*2; let c = a * b + 5; c;", 55},
  };

  for (auto &test : tests) {
    /*std::cerr << "Case input: " << test.input << "\n\texpected:\t" << test.expected << std::endl;*/
    auto *evaluated = test_eval(test.input);
    INFO("Case input: " << test.input << "\n\texpected:\t" << test.expected);
    INFO("\tevaluated:\t" << evaluated->inspect());
    /*std::cerr  << "\tevaluated:\t" << evaluated->inspect() << std::endl;;*/
    REQUIRE(evaluated);

    REQUIRE(test_integer_obj(evaluated, test.expected));
  }
}

TEST_CASE("Test Function Object", "[evaluator]") {
  INFO("### Test Function Object ###");

  std::string input{"fn(x) { x + 2; };"};

  auto *evaluated = test_eval(input);
  REQUIRE(evaluated);
  auto *fn = dynamic_cast<FunctionObj *>(evaluated);
  REQUIRE(fn);
  REQUIRE(fn->type() == ObjectType::FUNCTION_OBJ);
  REQUIRE(fn->parameters.size() == 1);
  REQUIRE(fn->parameters[0]->to_string() == "x");
  REQUIRE(fn->body->to_string() == "(x + 2)");
}

TEST_CASE("Test Function Application", "[evaluator]") {
  INFO("### Test Function Application ###");

  struct TestType {
    std::string input;
    int64_t expected;
  };

  TestType tests[] {
    {"let identity = fn(x) { x }; identity(5);", 5},
    {"let identity = fn(x) { return x; }; identity(5);", 5},
    {"let double = fn(x) { x * 2}; double(5);", 10},
    {"let add = fn(x, y) { x + y; }; add(5, 5);", 10},
    {"let add = fn(x, y) { x + y; }; add(5 + 5, add(5, 5))", 20},
    {"fn(x) { x; }(5);", 5},
  };

  for (auto &test : tests) {
    INFO("Case input: " << test.input << "\n\texpected:\t" << test.expected);
    auto *evaluated = test_eval(test.input);
    REQUIRE(evaluated);
    INFO("\tevaluated:\t" << evaluated->inspect());
    REQUIRE(test_integer_obj(evaluated, test.expected));
  }
}

TEST_CASE("Test Closures", "[evaluator]") {
  INFO("### Test Closures ###");

  std::string input{R"()
  let newAdder = fn(x) {
    fn(y) {x + y};
  };

  let addTwo = newAdder(2);
  addTwo(2);
  )"};

  REQUIRE(test_integer_obj(test_eval(input), 4));
}

TEST_CASE("Test Eval String Expression", "[evaluator]") {
  INFO("### Test Eval String Expression ###");

  std::string input{"\"Hello World!\""};

  auto evaluated = test_eval(input);
  INFO("evaluated: " << evaluated->inspect());
  REQUIRE(evaluated);
  auto str = dynamic_cast<StringObj *>(evaluated);
  REQUIRE(str);
  REQUIRE(str->value == "Hello World!");
}

TEST_CASE("Test String Concatenation", "[evaluator]") {
  INFO("### Test String Concatenation ###");
  std::cerr << "### Test String Concatenation ###" << std::endl;

  std::string input{"\"Hello\" + \" \" + \"World!\""};

  auto evaluated = test_eval(input);
  INFO("evaluated: " << evaluated->inspect());
  REQUIRE(evaluated);
  auto str = dynamic_cast<StringObj *>(evaluated);
  REQUIRE(str);
  REQUIRE(str->value == "Hello World!");
}
