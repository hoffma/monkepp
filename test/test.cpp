// #include <iostream>
// 
// #define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
// #include "catch.hpp"
#include <catch2/catch_test_macros.hpp>

unsigned int factorial(unsigned int number) {
  return number <= 1 ? number : factorial(number-1)*number;
}

TEST_CASE( "Simple Test", "[simple]" ) {
    REQUIRE(1 == 1);
}

TEST_CASE( "Simple Test fail", "[simple]" ) {
    REQUIRE(1 != 1);
}

TEST_CASE("Factorials are computed", "[factorial]") {
    REQUIRE(factorial(1) == 1);
    REQUIRE(factorial(2) == 2);
    REQUIRE(factorial(3) == 6);
    REQUIRE(factorial(10) == 3528800);
}
